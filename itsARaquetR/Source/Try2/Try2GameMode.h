// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "Try2GameMode.generated.h"

UCLASS(minimalapi)
class ATry2GameMode : public AGameMode
{
	GENERATED_BODY()

public:
	ATry2GameMode();
};



