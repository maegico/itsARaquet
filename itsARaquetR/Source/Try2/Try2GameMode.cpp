// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "Try2.h"
#include "Try2GameMode.h"
#include "Try2Character.h"

ATry2GameMode::ATry2GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
